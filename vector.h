/******************************************************************************/
/*
* @file   vector.h
* @author Aditya Harsh
* @brief  Smart vector functionality. Allows vectors of any type to be created.
*/
/******************************************************************************/

#pragma once

/* define boolean values */
typedef enum {false = 0, true = 1} bool;
/* opaque struct pointer */
typedef struct vector vector;
/* pointer to a vector */
typedef struct vector* vector_p;
/* typedef for printing function */
typedef void (*PRINTFUNC)(const vector*);

/* allocates a vector */
vector* vector_alloc(unsigned capacity, unsigned data_size, const char* data_type, PRINTFUNC func);
/* frees a vector */
void vector_free(vector** vec);
/* pushes back into a vector */
void* vector_push_back(vector** vec);
/* pushes onto the front of a vector */
void* vector_push_front(vector** vec);
/* pops the back of a vector */
void vector_pop_back(vector* vec);
/* pops the front of a vector */
void vector_pop_front(vector* vec);
/* gets an element */
void* vector_get(const vector* vec, unsigned index);
/* gets the beginning of the vector */
void* vector_get_begin(const vector* vec);
/* gets the end of the vector */
void* vector_get_end(const vector* vec);
/* removes an element at an index */
void vector_remove_element(vector* vec, unsigned index);
/* deep copies a vector */
void vector_copy(vector** destination, const vector* source);
/* clears a vector */
void vector_clear(vector* vec);
/* returns whether or not the vector is empty */
bool vector_empty(const vector* vec);
/* returns the size of the vector */
unsigned vector_size(const vector* vec);
/* returns the capacity of the vector */
unsigned vector_capacity(const vector* vec);
/* matches the capacity of a vector to its size */
void vector_shrink_to_fit(vector** vec);
/* prints the data type of a vector */
void vector_print_data_type(const vector* vec);
/* dumps the contents of a vector */
void vector_dump(const vector* vec);
/* checks the type of the vector */
bool vector_check_type(const vector* vec, const char* type);
