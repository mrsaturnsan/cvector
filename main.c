#include "vector.h"
#include <stdio.h>
#include <stdlib.h>

#define UNUSED(x) (void)(x)

void print_vector(const vector* vec)
{
    unsigned i;

    for (i = 0; i < vector_size(vec); ++i)
    {
        printf("%f\n",*(float*)vector_get(vec, i));
    }
}

int main(void)
{
    unsigned i;
    
    /* allocate memory */
    vector_p my_vec = vector_alloc(0, sizeof(float), "float", print_vector);
    /* copy test */
    vector_p vec2 = NULL;

    for (i = 10; i <= 20; ++i)
        *(float*)vector_push_back(&my_vec) = 1.2f * i;
        
    /* copy the first vector into the second */
    vector_copy(&vec2, my_vec);

    for (i = vector_size(vec2); i > 0 ; --i)
        vector_pop_back(vec2);

    vector_dump(vec2);

    /* free memory */
    vector_free(&my_vec);
    vector_free(&vec2);

    return 0;
}