CC = gcc
CFLAGS = -Wall -Wextra -Werror -ansi -pedantic -g -O3
FILE = main.c vector.c
OUT = output

multi:
	@$(MAKE) -j8 build

build:
	@$(CC) $(CFLAGS) $(FILE) -o $(OUT)

run:
	@./$(OUT)

clean:
	@rm $(OUT)