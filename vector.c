/******************************************************************************/
/*
* @file   vector.h
* @author Aditya Harsh
* @brief  Smart vector functionality. Allows vectors of any type to be created.
*/
/******************************************************************************/

#include "vector.h"   /* vector interface */
#include <stdlib.h>   /* malloc, calloc, free */
#include <stdio.h>    /* printf */
#include <string.h>   /* memcpy, strcpy, strcmp */

/* default capacity is set to two */
#define DEFAULT_CAPACITY 2

/* 1 byte offset */
typedef unsigned char uchar;

/**
 * @brief Vector struct (40 bytes)
 * 
 */
struct vector
{
    /* the array of data stored inside the vector */
    void* data_;
    /* debug functionality */
    char* data_type_; /* the type of data being held */
    PRINTFUNC pf_;    /* callback function to print the vector */

    /* the number of elements */
    unsigned size_;
    /* the max number of elements */
    unsigned capacity_;
    /* the size of the data */
    unsigned data_size_;
};

/**
 * @brief Allocates a vector of any type of elements.
 * 
 * @param capacity
 * @param data_size
 * @param data_type
 * @param func
 */
vector* vector_alloc(unsigned capacity, unsigned data_size, const char* data_type, PRINTFUNC func)
{
    /* allocate the data */
    vector* vec = malloc(sizeof(vector));

    /* check if allocation succeeded */
    if (vec)
    {
        /* allocate memory (0 = default capacity) */
        if (capacity)
        {
            vec->data_ = calloc(capacity, data_size);
            vec->capacity_ = capacity;
        }
        else
        {
            vec->data_ = calloc(DEFAULT_CAPACITY, data_size);
            vec->capacity_ = DEFAULT_CAPACITY;
        }

        /* check for successful allocation */
        if (!vec->data_)
        {
            free(vec);
            return NULL;
        }

        /* store the name of the type */
        vec->data_type_ = malloc(sizeof(char) * (strlen(data_type) + 1));

        /* free allocated memory */
        if (!vec->data_type_)
        {
            free(vec->data_);
            free(vec);
            return NULL;
        }

        /* set values */
        strcpy(vec->data_type_, data_type);
        vec->size_ = 0;
        vec->data_size_ = data_size;
        vec->pf_ = func;

        return vec;
    }

    /* failure */
    return NULL;
}

/**
 * @brief Frees allocated memory.
 * 
 * @param vec
 * @param preserve_data
 */
void vector_free(vector** vec)
{
    if (vec && *vec)
    {
        free((*vec)->data_type_);
        free((*vec)->data_);
        free(*vec);
        *vec = NULL;
    }
}

/**
 * @brief Pushes back.
 * 
 * @param vec 
 * @param data 
 */
void* vector_push_back(vector** vec)
{
    /* used for allocating additional memory if necessary */
    vector* temp = NULL;

    if (!vec || !(*vec)) return NULL;
    
    if ((*vec)->size_ == (*vec)->capacity_)
    {
        temp = vector_alloc((*vec)->capacity_ * 2, (*vec)->data_size_, (*vec)->data_type_, (*vec)->pf_);

        if (temp)
        {
            /* copy values */
            vector_copy(&temp, *vec);

            /* set */
            ++temp->size_;

            /* free the old vector */
            vector_free(vec);

            /* set new */
            *vec = temp;
        }
        else
        {
            return NULL;
        }
    }
    else
    {
        /* increment size */
        ++(*vec)->size_;
    }

    return (uchar*)(*vec)->data_ + ((*vec)->data_size_ * ((*vec)->size_ - 1));
}

/**
 * @brief Pushes front.
 * 
 * @param vec 
 * @param data 
 */
void* vector_push_front(vector** vec)
{
    /* iter */
    int i;
    /* used for allocating additional memory if necessary */
    vector* temp = NULL;

    if (!vec || !(*vec)) return NULL;

    if ((*vec)->size_ == (*vec)->capacity_)
    {
        temp = vector_alloc((*vec)->capacity_ * 2, (*vec)->data_size_, (*vec)->data_type_, (*vec)->pf_);

        if (temp)
        {
            /* copy values */
            vector_copy(&temp, *vec);

            for (i = (*vec)->size_; i >= 0; --i)
                *((uchar*)temp->data_ + ((*vec)->data_size_ * (i + 1))) = *((uchar*)temp->data_ + ((*vec)->data_size_ * i));

            /* set */
            ++temp->size_;

            /* free the old vector */
            vector_free(vec);

            /* set new */
            *vec = temp;
        }
        else
        {
            return NULL;
        }
    }
    else
    {
        for (i = (*vec)->size_; i >= 0; --i)
            *((uchar*)temp->data_ + ((*vec)->data_size_ * (i + 1))) = *((uchar*)temp->data_ + ((*vec)->data_size_ * i));
        ++(*vec)->size_;
    }

    /* return the front */
    return (*vec)->data_;
}

/**
 * @brief "Removes" an element from the back.
 * 
 * @param vec 
 */
void vector_pop_back(vector* vec)
{
    if (!vec) return;

    if (vec->size_)
       --vec->size_; 
}

/**
 * @brief Removes the first element, and shifts the vector.
 * 
 * @param vec 
 */
void vector_pop_front(vector* vec)
{
    if (!vec) return;

    /* remove an element and shift everything */
    vector_remove_element(vec, 0);
}

/**
 * @brief Copies from one vector into another.
 * 
 * @param destination 
 * @param source 
 */
void vector_copy(vector** destination, const vector* source)
{
    /* safety check */
    if (!destination || !source) return;

    /* allocate additional memory if necessary */
    if (!(*destination) || (*destination)->capacity_ < source->size_)
    {
        vector_free(destination);
        *destination = vector_alloc(source->size_, source->data_size_, source->data_type_, source->pf_);
        if (!(*destination)) return;
    }

    /* copy data */
    memcpy((*destination)->data_, source->data_, source->data_size_ * source->size_);

    /* set the size */
    (*destination)->size_ = source->size_;
}

/**
 * @brief "Clears" a vector.
 * 
 * @param vec 
 */
void vector_clear(vector* vec)
{
    if (!vec) return;

    /* set the size to 0 */
    vec->size_ = 0;
}

/**
 * @brief Returns whether or not a vector is empty.
 * 
 * @param vec 
 * @return true 
 * @return false 
 */
bool vector_empty(const vector* vec)
{
    if (!vec) return true;

    return vec->size_ ? false : true;
}

/**
 * @brief Returns the size of a vector.
 * 
 * @param vec 
 * @return unsigned 
 */
unsigned vector_size(const vector* vec)
{
    if (!vec) return 0;

    return vec->size_;
}

/**
 * @brief Returns the capacity of a vector.
 * 
 * @param vec 
 * @return unsigned 
 */
unsigned vector_capacity(const vector* vec)
{
    if (!vec) return 0;

    return vec->capacity_;
}

/**
 * @brief Resizes the vector to not waste memory.
 * 
 * @param vec 
 */
void vector_shrink_to_fit(vector** vec)
{
    vector* temp = NULL;

    if (!vec || !(*vec)) return;

    if ((*vec)->capacity_ > (*vec)->size_)
    {
        temp = vector_alloc((*vec)->size_, (*vec)->data_size_, (*vec)->data_type_, (*vec)->pf_);
        if (temp)
        {
            vector_copy(&temp, *vec);
            vector_free(vec);
            *vec = temp;
        }
    }
}

/**
 * @brief Removes an element from the vector.
 * 
 * @param vec 
 */
void vector_remove_element(vector* vec, unsigned index)
{
    /* iterator variable */
    unsigned i;

    /* safety checking */
    if (!vec || index >= vec->size_) return;

    /* shift elements */
    for (i = index; i < (vec->size_ - 1); ++i)
        *((uchar*)vec->data_ + (i * vec->data_size_)) = *((uchar*)vec->data_ + ((i + 1) * vec->data_size_));

    --vec->size_;
}

/**
 * @brief Gets a requested value.
 * 
 * @param vec 
 * @param index 
 * @return void* 
 */
void* vector_get(const vector* vec, unsigned index)
{
    if (!vec || index >= vec->size_) return NULL;

    return (uchar*)vec->data_ + (index * vec->data_size_);
}

/**
 * @brief Prints the type of data held within the vector.
 * 
 * @param vec 
 */
void vector_print_data_type(const vector* vec)
{
    if (!vec) return;
    printf("%s\n", vec->data_type_);
}

/**
 * @brief Dumps the contents of a vector.
 * 
 * @param vec 
 */
void vector_dump(const vector* vec)
{
    if (!vec || !vec->pf_) return;

    /* execute print function */
    vec->pf_(vec);
}

/**
 * @brief Checks a type of the vector.
 * 
 * @param vec 
 * @param type 
 * @return true 
 * @return false 
 */
bool vector_check_type(const vector* vec, const char* type)
{
    if (!vec || !type) return false;

    return (!strcmp(vec->data_type_, type));
}

/**
 * @brief Gets the first element of a vector.
 * 
 * @param vec 
 * @return void* 
 */
void* vector_get_begin(const vector* vec)
{
    return (!vec) ? NULL : vector_get(vec, 0);
}

/**
 * @brief Gets the last element of a vector.
 * 
 * @param vec 
 * @return void* 
 */
void* vector_get_end(const vector* vec)
{
    return (!vec || vec->size_ == 0) ? NULL : vector_get(vec, vec->size_ - 1);
}